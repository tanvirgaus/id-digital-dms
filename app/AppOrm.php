<?php declare(strict_types = 1);

namespace App;

use Dms\Core\Persistence\Db\Mapping\Definition\Orm\OrmDefinition;
use Dms\Core\Persistence\Db\Mapping\Orm;
use Dms\Web\Laravel\Persistence\Db\DmsOrm;

/**
 * TG - User Defined Entity and Mappers
 */
use App\Domain\Entities\TodoItem;
use App\Infrastructure\Persistence\TodoItemMapper;

use App\Domain\Entities\Company;
use App\Infrastructure\Persistence\CompanyMapper;

/**
 * The application's orm.
 *
 * @author Elliot Levin <elliotlevin@hotmail.com>
 */
class AppOrm extends Orm
{
    /**
     * Defines the object mappers registered in the orm.
     *
     * @param OrmDefinition $orm
     *
     * @return void
     */
    protected function define(OrmDefinition $orm)
    {
        $orm->enableLazyLoading();

        $orm->encompass(DmsOrm::inDefaultNamespace());

        // Register custom mappers
        $orm->entities([
            TodoItem::class => TodoItemMapper::class,
            Company::class => CompanyMapper::class
        ]);
    }
}