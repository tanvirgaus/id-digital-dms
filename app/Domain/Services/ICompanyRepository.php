<?php declare(strict_types = 1);

namespace App\Domain\Services\Persistence;

use Dms\Core\Model\ICriteria;
use Dms\Core\Model\ISpecification;
use Dms\Core\Persistence\IRepository;
use App\Domain\Entities\Company;

/**
 * The repository for the App\Domain\Entities\Company entity.
 */
interface ICompanyRepository extends IRepository
{
    /**
     * {@inheritDoc}
     *
     * @return Company[]
     */
    public function getAll() : array;

    /**
     * {@inheritDoc}
     *
     * @return Company
     */
    public function get($id);

    /**
     * {@inheritDoc}
     *
     * @return Company[]
     */
    public function getAllById(array $ids) : array;

    /**
     * {@inheritDoc}
     *
     * @return Company|null
     */
    public function tryGet($id);

    /**
     * {@inheritDoc}
     *
     * @return Company[]
     */
    public function tryGetAll(array $ids) : array;

    /**
     * {@inheritDoc}
     *
     * @return Company[]
     */
    public function matching(ICriteria $criteria) : array;

    /**
     * {@inheritDoc}
     *
     * @return Company[]
     */
    public function satisfying(ISpecification $specification) : array;
}