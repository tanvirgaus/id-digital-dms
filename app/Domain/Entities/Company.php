<?php declare(strict_types = 1);

namespace App\Domain\Entities;

use Dms\Core\Model\Object\ClassDefinition;
use Dms\Core\Model\Object\Entity;

/**
 * TG - Adding Geo address location for the address field
 */
use Dms\Common\Structure\Web\EmailAddress;
use Dms\Common\Structure\Geo\StreetAddress;

/**
 * Your first entity.
 *
 * An item on the Company list.
 */
class Company extends Entity
{
    // Define constants for property names
    const NAME = 'name';
    const ADDRESS = 'address';
    const CONTACT_PERSON = 'contactPerson';
    const PHONE = 'phone';
    const EMAIL = 'email';
    const INDUSTRY = 'industry';
    const IS_ACTIVE = 'isActive';


    /**
     * @var string
     */
    public $name, $address, $contactPerson, $phone, $email, $industry;
    /**
     * @var bool
     */
     public $isActive;

    /**
     * Initialises a new TODO item.
     *
     * @param string $description
     */
    public function __construct(string $name)
    {
        parent::__construct();
        $this->name = $name;
        $this->isActive   = true;
    }

    /**
     * Defines the structure of this entity.
     *
     * @param ClassDefinition $class
     */
    protected function defineEntity(ClassDefinition $class)
    {
        // Enables strong typing for this entity
        $class->property($this->name)->asString();
        $class->property($this->address)->nullable()->asObject(StreetAddress::class);
        $class->property($this->contactPerson)->nullable()->asString();
        $class->property($this->phone)->nullable()->asString();
        $class->property($this->email)->nullable()->asObject(EmailAddress::class);
        $class->property($this->industry)->nullable()->asString();
        $class->property($this->isActive)->asBool();
    }
}