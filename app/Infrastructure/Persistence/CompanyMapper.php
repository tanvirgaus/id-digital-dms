<?php declare(strict_types = 1);

namespace App\Infrastructure\Persistence;

use Dms\Core\Persistence\Db\Mapping\Definition\MapperDefinition;
use Dms\Core\Persistence\Db\Mapping\EntityMapper;
use App\Domain\Entities\Company;

use Dms\Common\Structure\Geo\Persistence\StreetAddressMapper;
use Dms\Common\Structure\Web\Persistence\EmailAddressMapper;
/**
 * Your first entity mapper.
 *
 * The App\Domain\Entities\Company entity mapper.
 */
class CompanyMapper extends EntityMapper
{
    /**
     * Defines the entity mapper
     *
     * @param MapperDefinition $map
     *
     * @return void
     */
    protected function define(MapperDefinition $map)
    {
        $map->type(Company::class);
        $map->toTable('companies');

        $map->idToPrimaryKey('id');

        $map->property(Company::NAME)->to('name')->asVarchar(191);
        $map->embedded(Company::ADDRESS)
                   ->using(new StreetAddressMapper('address'));

        $map->property(Company::CONTACT_PERSON)->to('contact_person')->nullable()->asVarchar(191);
        $map->property(Company::PHONE)->to('phone')->nullable()->asVarchar(191);
        $map->embedded(Company::EMAIL)
            ->using(new EmailAddressMapper('email'));
        $map->property(Company::INDUSTRY)->to('industry')->nullable()->asVarchar(191);
        $map->property(Company::IS_ACTIVE)->to('is_active')->asBool();
    }
}