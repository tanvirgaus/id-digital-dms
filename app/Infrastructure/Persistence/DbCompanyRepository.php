<?php declare(strict_types = 1);

namespace App\Infrastructure\Persistence;

use Dms\Core\Persistence\Db\Connection\IConnection;
use Dms\Core\Persistence\Db\Mapping\IOrm;
use Dms\Core\Persistence\DbRepository;
use App\Domain\Services\Persistence\ICompanyRepository;
use App\Domain\Entities\Company;

/**
 * The database repository implementation for the App\Domain\Entities\Company entity.
 */
class DbCompanyRepository extends DbRepository implements ICompanyRepository
{
    public function __construct(IConnection $connection, IOrm $orm)
    {
        parent::__construct($connection, $orm->getEntityMapper(Company::class));
    }

    // Custom queries can go here...
}