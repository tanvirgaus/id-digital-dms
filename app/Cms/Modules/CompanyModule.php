<?php declare(strict_types = 1);

namespace App\Cms\Modules;

use Dms\Core\Auth\IAuthSystem;
use Dms\Core\Common\Crud\CrudModule;
use Dms\Core\Common\Crud\Definition\CrudModuleDefinition;
use Dms\Core\Common\Crud\Definition\Form\CrudFormDefinition;
use Dms\Core\Common\Crud\Definition\Table\SummaryTableDefinition;
use App\Domain\Services\Persistence\ICompanyRepository;
use App\Domain\Entities\Company;
use Dms\Common\Structure\Field;

/**
 * The company module.
 */
class CompanyModule extends CrudModule
{
    public function __construct(ICompanyRepository $dataSource, IAuthSystem $authSystem)
    {
        parent::__construct($dataSource, $authSystem);
    }

    /**
     * Defines the structure of this module.
     *
     * @param CrudModuleDefinition $module
     */
    protected function defineCrudModule(CrudModuleDefinition $module)
    {
        $module->name('company');

        $module->labelObjects()->fromProperty(Company::NAME);

        $module->metadata([
            'icon' => 'list', // Choose icon from http://fontawesome.io/icons/
        ]);

        $module->crudForm(function (CrudFormDefinition $form) {
            $form->section('Details', [
                $form->field(
                    Field::create('name', 'Name')->string()->required()
                )->bindToProperty(Company::NAME),
                $form->field(
                    Field::create('address', 'Address')->streetAddress()
                )->bindToProperty(Company::ADDRESS),
                $form->field(
                    Field::create('contact_person', 'Contact Person')->string()
                )->bindToProperty(Company::CONTACT_PERSON),
                $form->field(
                    Field::create('phone', 'Phone')->string()
                )->bindToProperty(Company::PHONE),
                $form->field(
                    Field::create('email', 'Email')->email()
                )->bindToProperty(Company::EMAIL),
                $form->field(
                    Field::create('industry', 'Industry')->string()
                )->bindToProperty(Company::INDUSTRY),
                $form->field(
                    Field::create('is_active', 'Active')->bool()
                )->bindToProperty(Company::IS_ACTIVE),
            ]);
        });

        $module->removeAction()->deleteFromDataSource();

        $module->summaryTable(function (SummaryTableDefinition $table) {
            $table->mapProperty(Company::NAME)->to(Field::create('name', 'Name')->string()->required());
            $table->mapProperty(Company::ADDRESS)->to(Field::create('address', 'Address')->streetAddress()->required());
            $table->mapProperty(Company::CONTACT_PERSON)->to(Field::create('contact_person', 'Contact')->string()->required());
            $table->mapProperty(Company::PHONE)->to(Field::create('phone', 'Phone')->string()->required());
            $table->mapProperty(Company::EMAIL)->to(Field::create('email_address', 'Email Address')->email());
            $table->mapProperty(Company::INDUSTRY)->to(Field::create('industry', 'Industry')->string()->required());
            $table->mapProperty(Company::IS_ACTIVE)->to(Field::create('is_active', 'Active')->bool());

            $table->view('all', 'All')
                ->loadAll()
                ->asDefault();
        });
    }
}