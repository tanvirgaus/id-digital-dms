<?php declare(strict_types = 1);

namespace App\Cms;

use Dms\Core\Package\Definition\PackageDefinition;
use Dms\Core\Package\Package;
use App\Cms\Modules\CompanyModule;

/**
 * The Company package.
 */
class CompanyPackage extends Package
{
    /**
     * Defines the structure of this cms package.
     *
     * @param PackageDefinition $package
     *
     * @return void
     */
    protected function define(PackageDefinition $package)
    {
        $package->name('company');

        $package->metadata([
            'icon' => 'address-book', // Choose icon from http://fontawesome.io/icons/
        ]);

        $package->modules([
            'company' => CompanyModule::class,
        ]);
    }
}