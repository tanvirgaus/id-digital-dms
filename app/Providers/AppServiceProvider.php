<?php

namespace App\Providers;

use App\AppCms;
use App\AppOrm;
use Dms\Core\ICms;
use Dms\Core\Persistence\Db\Mapping\IOrm;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

/**
 * TG - Adding Custom Repositories
 */

use App\Domain\Services\Persistence\ITodoItemRepository;
use App\Infrastructure\Persistence\DbTodoItemRepository;

use App\Domain\Services\Persistence\ICompanyRepository;
use App\Infrastructure\Persistence\DbCompanyRepository;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(IOrm::class, AppOrm::class);
        $this->app->singleton(ICms::class, AppCms::class);

        // Bind custom repositories here
        $this->app->singleton(ITodoItemRepository::class, DbTodoItemRepository::class);
        $this->app->singleton(ICompanyRepository::class, DbCompanyRepository::class);
    }
}
