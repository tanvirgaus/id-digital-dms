<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTodoItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dms_analytics', function (Blueprint $table) {
            $table->string('driver', 191)->change();
        });

        Schema::table('dms_password_resets', function (Blueprint $table) {
            $table->string('email', 191)->change();
            $table->string('token', 191)->change();
        });

        Schema::table('dms_roles', function (Blueprint $table) {
            $table->string('name', 191)->change();
        });

        Schema::table('dms_permissions', function (Blueprint $table) {
            $table->string('name', 191)->change();
        });

        Schema::table('dms_temp_files', function (Blueprint $table) {
            $table->string('token', 191)->change();
            $table->string('client_file_name', 191)->nullable()->change();
        });

        Schema::table('dms_users', function (Blueprint $table) {
            $table->string('full_name', 191)->change();
            $table->string('email', 191)->change();
            $table->string('username', 191)->change();
            $table->string('password_hash', 191)->nullable()->change();
            $table->string('password_algorithm', 191)->nullable()->change();
            $table->string('remember_token', 191)->nullable()->change();
            $table->string('oauth_provider_name', 191)->nullable()->change();
            $table->string('oauth_account_id', 191)->nullable()->change();
        });

        Schema::create('todo_items', function (Blueprint $table) {
            $table->integer('id')->autoIncrement()->unsigned();
            $table->string('description', 191);
            $table->boolean('completed');

        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dms_roles', function (Blueprint $table) {
            $table->string('name', 191)->change();
        });

        Schema::table('dms_users', function (Blueprint $table) {
            $table->string('full_name', 191)->change();
            $table->string('email', 191)->change();
            $table->string('username', 191)->change();
            $table->string('password_hash', 191)->nullable()->change();
            $table->string('password_algorithm', 191)->nullable()->change();
            $table->string('remember_token', 191)->nullable()->change();
            $table->string('oauth_provider_name', 191)->nullable()->change();
            $table->string('oauth_account_id', 191)->nullable()->change();
        });

        Schema::table('dms_permissions', function (Blueprint $table) {
            $table->string('name', 191)->change();
        });

        Schema::table('dms_password_resets', function (Blueprint $table) {
            $table->string('email', 191)->change();
            $table->string('token', 191)->change();
        });

        Schema::table('dms_temp_files', function (Blueprint $table) {
            $table->string('token', 191)->change();
            $table->string('client_file_name', 191)->nullable()->change();
        });

        Schema::table('dms_analytics', function (Blueprint $table) {
            $table->string('driver', 191)->change();
        });

        Schema::drop('todo_items');

    }
}
